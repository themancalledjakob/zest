/**
 * Manager
 * all managers do this
 */

class Manager {

  int val[];      // Data received
  int dmxVal[];
  float smoothVal[]; 
  float customSmoothVal[]; 
  long total;
  long totalIndy[];

  Conf conf = new Conf();

  class Conf {
    float smoothing = 0.95;
    int min = 0;
    int max = 60;
    int values = 3;
  }

  public void setup(){
    val = new int[conf.values];
    dmxVal = new int[conf.values]; 
    smoothVal = new float[conf.values];
    customSmoothVal = new float[conf.values];
    totalIndy = new long[conf.values];
  }
  
  public void update(){

    for(int i = 0; i < conf.values; ++i){
      smoothVal[i] = smoothVal[i] * conf.smoothing + (1.0-conf.smoothing) * val[i];
    }
  }

  public void update(boolean real){

  }

  int[] getRawValues()
  {
    return val;
  }

  long getTotal(){
    return total;
  }

  long[] getTotalIndy(){
    return totalIndy;
  }

  int[] getDmxValues(){
    dmxVal[0] = int(map(val[0], conf.min, conf.max, 0, 255));
    dmxVal[1] = int(map(val[1], conf.min, conf.max, 0, 255));
    dmxVal[2] = int(map(val[2], conf.min, conf.max, 0, 255));
    return dmxVal;
  }

  float[] getSmoothValues(){
    return getSmoothValues(false);
  }

  float[] getSmoothValues(boolean mapped)
  {
    float out[] = new float[conf.values];

    for(int i = 0; i < conf.values; ++i){
      if(mapped) {
        out[i] = map(smoothVal[i], conf.min, conf.max, 0.0, 1.0);
      } else {
        out[i] = smoothVal[i];
      }
    }

    return out;
  }

    float[] getSmoothValues(boolean mapped, float customSmoothing)
  {
    float out[] = new float[conf.values];

    for(int i = 0; i < conf.values; ++i){
      customSmoothVal[i] = customSmoothVal[i] * customSmoothing + (1.0-customSmoothing) * val[i];
      out [i] = customSmoothVal[i];

      if(mapped) {
        out[i] = map(out[i], conf.min, conf.max, 0.0, 1.0);
      }
    }

    return out;
  }

  float getSmoothValueSum(){
    float out = 0;
    for(int i = 0; i < conf.values; ++i){
      out += getSmoothValues(true)[i];
    }
    return out/(float)conf.values;
  }
}

uniform sampler2D colorMap;
uniform sampler2D colorMap2;

uniform float interpol;
uniform float fade;

varying vec4 vertTexCoord;

varying vec4 vertColor;

uniform float time;
// uniform vec3 indyTime;
// varying float location;

void main() {
	vec4 vertTexCoord2 = vertTexCoord;
	vertTexCoord2.t = mod( time * 0.0321 + vertTexCoord.t,1.0);
  gl_FragColor = texture2D(colorMap, vertTexCoord2.st) * (1.0-interpol) + interpol * texture2D(colorMap2, vertTexCoord.st);
  gl_FragColor.a = fade;
  gl_FragColor.rgb = gl_FragColor.rgb * vertColor.rgb;
}

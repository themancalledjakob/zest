
#define PROCESSING_TEXLIGHT_SHADER

uniform mat4 modelview;
uniform mat4 transform;
uniform mat3 normalMatrix;
uniform mat4 texMatrix;

uniform int lightCount;
uniform vec4 lightPosition[8];
uniform vec3 lightNormal[8];
uniform vec3 lightAmbient[8];
uniform vec3 lightDiffuse[8];
uniform vec3 lightSpecular[8];
uniform vec3 lightFalloff[8];
uniform vec2 lightSpot[8];

uniform float twist;

uniform float movement;
uniform float interpol;

attribute vec4 vertex;
attribute vec4 color;
attribute vec3 normal;
attribute vec2 texCoord;

attribute vec4 ambient;
attribute vec4 specular;
attribute vec4 emissive;
attribute float shininess;

varying vec4 vertColor;
varying vec4 vertTexCoord;

const float zero_float = 0.0;
const float one_float = 1.0;
const vec3 zero_vec3 = vec3(0);

// uniform sampler2D displacementMap;
uniform float displaceStrength;

uniform float time;
// uniform float time2;
uniform vec2 resolution;
uniform vec2 mouse;

uniform vec3 parts;

// varying float location;

// uniform float translateMe;

float ripple(float dist, float shift)
{
  return cos(64.0 * dist + shift) / (1.0 + 1.0 * dist);
}


#define POLES 21

#define REFLECTIONS 10.0

vec4 cymatics(vec4 pseudo_FragCoord){

  float larger = max(resolution.x, resolution.y);
  vec2 uv = (pseudo_FragCoord.xy - .5*resolution.xy) / larger;
  vec2 uvflip = vec2(uv.x, -uv.y);
  vec2 cursor = (mouse.xy - .5*resolution.xy) / larger;
  vec2 blessr = vec2(-cursor.x, cursor.y);
  
  //float on = float(abs(uv.x)<.25 && abs(uv.y)<.25);
  
  float lum = .5 +
    /*.1 * ripple(length(cursor - uv), -time) +
    .1 * ripple(length(blessr - uv), -time) +
    .1 * ripple(length(cursor - uvflip), -time) +
    .1 * ripple(length(blessr - uvflip), -time) +*/
    .1 * ripple(length(uv), 0.0) +
    //.1 * cos(64.0*uv.y - time) +
    //.1 * cos(64.0*(uv.x*uv.x) - time) +
    0.0;
  
  float twopi = 2.0*3.141592654;
  const int count = POLES;
  float fcount = float(count);
  vec2 rot = vec2(cos(twopi*.618), sin(twopi*.618));
  vec2 tor = vec2(-sin(twopi*.618), cos(twopi*.618));
  for (int i = 0; i < count; ++i)
  {
    lum += .2 * ripple(length(cursor - uv), -time);
    cursor = cursor.x*rot + cursor.y*tor;
  }
  
  /*float lum = .5, dist;
  vec2 part, flip = vec2(1.0, 1.0);
  
  //float freq = 64.0, phase = -time;
  float freq = 32.0, phase  = 0.0; // * pow(4.0, cos(time/8.0)), phase = 0.0;
  
  for (float ox = -REFLECTIONS; ox <= REFLECTIONS; ox += 1.0)
  {
    for (float oy = -REFLECTIONS; oy <= REFLECTIONS; oy += 1.0)
    {
      dist = length((cursor*flip-uv)+vec2(ox, oy));
      lum += cos(freq * dist - phase) / (5.0 + 10.0*dist);
      
      flip.y *= -1.0;
    }
    flip.x *= -1.0;
  }*/
  
  lum = 3.0*lum*lum - 2.0*lum*lum*lum;
  lum = max(0.0,min(1.0,lum));
  return vec4(lum, lum, lum, 1.0);
  
  
  /*fragColor = vec4(.5+.5*sin(3000.0*time),
    .5+.5*sin(4997.0*time+resolution.x*3910.0),
    .5+.5*cos(2872.0*time+resolution.y*8721.0), 1.0);*/
}

float falloffFactor(vec3 lightPos, vec3 vertPos, vec3 coeff) {
  vec3 lpv = lightPos - vertPos;
  vec3 dist = vec3(one_float);
  dist.z = dot(lpv, lpv);
  dist.y = sqrt(dist.z);
  return one_float / dot(dist, coeff);
}

float spotFactor(vec3 lightPos, vec3 vertPos, vec3 lightNorm, float minCos, float spotExp) {
  vec3 lpv = normalize(lightPos - vertPos);
  vec3 nln = -one_float * lightNorm;
  float spotCos = dot(nln, lpv);
  return spotCos <= minCos ? zero_float : pow(spotCos, spotExp);
}

float lambertFactor(vec3 lightDir, vec3 vecNormal) {
  return max(zero_float, dot(lightDir, vecNormal));
}

float blinnPhongFactor(vec3 lightDir, vec3 vertPos, vec3 vecNormal, float shine) {
  vec3 np = normalize(vertPos);
  vec3 ldp = normalize(lightDir - np);
  return pow(max(zero_float, dot(ldp, vecNormal)), shine);
}

// const float pi = 3.1415926536;

// float shadertoyDots( vec2 fragCoordinates, vec3 iResolution, float iGlobalTime )
// {
//     float s=iGlobalTime*2.1;
//     float e=floor(s*0.25);
//     float d=fract(sin(e*7584.0)+5463.13);
    
//     float focus = 256.0+4096.0*d;
//     vec3 eye = vec3(0.0,0.0,-focus);
//     vec3 ray = vec3((fragCoordinates.xy-iResolution.xy*0.5)*2.0,-eye.z);

//     float ax = e*pi-sin(s*.5)*1.2;
//     float ay = e*pi+sin(s*d)*pi/6.0;
    
//     vec3 pn = vec3(0.0,-sin(ax),cos(ax));
    
//     vec3 p = eye-ray*dot(eye,pn)/dot(ray,pn);
    
//     p = vec3(p.x*cos(ay)-p.y*sin(ay),p.x*sin(ay)+p.y*cos(ay),p.z);

//     float q=((1.0-d)*0.1+d*0.9);
    
//     vec2 cs = vec2(q*256.0,q*256.0);
//     vec2 co = vec2(0.0,iGlobalTime*6.0);
//     vec3 color = vec3(0.0);
    
//     float size = (1.0-fract(s))*128.0*q;
    
//     float col = (
//         clamp(size-distance(
//             mod(p.xy+co*cs.x,cs),cs.xy*0.5
//         ),0.0,1.0)*clamp(focus/(p.z-eye.z),0.0,1.0)
//     );

//   return col*min(1.0,iGlobalTime*0.25);
// }

// Quasicrystal based on Matt Henderson's blog post:
// http://blog.matthen.com/post/51566631087/quasicrystals-are-highly-structured-patterns-which  
// click and drag the mouse for interactivity.
// Left/Right: scale
// Up/Down: number of waves

// normalized mouse position. Default values
// float mouseX = 0.8;
// float mouseY = 0.4;

// float shadertoyBlurthing( vec2 fragCoordinates, vec3 iResolution, vec2 iMouse, float iGlobalTime )
// {
//   if( iMouse.x != 0.0 ) { // once Shadertoy mouse pos is initialized use that:
//     mouseX = float(iMouse.x) / float(iResolution.x);
//     mouseY = float(iMouse.y) / float(iResolution.y);
//   }
  
//   float speed = 2.0;
//   float scale = 1.0; // watch out, this is not really a definition okay
//   const int N_MAX = 20;
//   float n = 7.0;
  
//   // click and drag up and down to change the number of symmetries (up is more)
//   // n is the number of waves. we allow a fractional number and fade out the final one below
//   n = mouseY * float(N_MAX);
  
//   // click and drag left and right to adjust scale (left zooms out)
//   scale = 0.1 + (1.0-mouseX);

//   scale *= 100.0;
  
//   const float pi = 3.1415926;
  
//   // accumulate n waves
//   float S = 0.0;
//   for( int i=0; i < N_MAX; ++i){
  
//     // allow for n+1 waves
//     if( i >= (int(n)+1) )
//       break;
    
//     float theta = pi * float(i)/float(n);
      
//     // center coordinates in viewport
//     float x = fragCoordinates.x - (iResolution.x*0.5);
//     float y = fragCoordinates.y - (iResolution.y*0.5);
    
//     float wp = x*cos(theta) + y*sin(theta);
//     float w = sin(wp*scale + iGlobalTime*speed);
    
//     // fade out the final wave in proportion to n-floor(n)
//     if( i == int(n) )
//       w *= n-floor(n);    
    
//     // accumulate 
//     S += 3.0 * w / n;
//   }
  
//   // sin/cos hackery to get some colours
//   // fragColor = vec4(sin(S-0.1)*.6,cos(S+.3)*.9,cos(S+.1)*1.1,1.0);
//   // greyscale version:
//   return S;
// }

void main() {
  // Calculating texture coordinates, with r and q set both to one
  vertTexCoord = texMatrix * vec4(texCoord, 1.0, 1.0);

  // vec4 dv = texture2D( displacementMap, vertTexCoord.st ); // rgba color of displacement map
  float df;// = 0.30*dv.r + 0.59*dv.g + 0.11*dv.b; // brightness calculation to create displacement float from rgb values
  float location =
    pow(max(0.0, vertTexCoord.x * 2.0 - 1.0),4.0) * parts.x +    // 0 0 1
    pow((1.0-abs((vertTexCoord.x * 2.0)-1.0)),3.0) * parts.y +   // 0 1 0
    pow((1.0-min(1.0,vertTexCoord.x * 2.0)),4.0)*parts.z;        // 1 0 0
  location = min(1.0,location*0.6);

  df = cymatics(vertTexCoord).x * location;// * location;// * vertTexCoord.x;
  vec4 newVertexPos = vertex + vec4(normal * df * displaceStrength, 0.0); // regular vertex position + direction * displacementMap * displaceStrength


float rot = newVertexPos.z * twist;
	mat3 rotmat = mat3(
	vec3(cos(rot),-sin(rot),0.0),
	vec3(sin(rot),cos(rot),0),
	vec3(0.0,0.0,1.0));


  // Vertex in clip coordinates
  gl_Position = transform * newVertexPos;
  // gl_Position.z -= translateMe;
    
  // Vertex in eye coordinates
  vec3 ecVertex = vec3(modelview * vertex);
  
  // Normal vector in eye coordinates
  vec3 ecNormal = normalize(normalMatrix * normal);
  
  if (dot(-one_float * ecVertex, ecNormal) < zero_float) {
    // If normal is away from camera, choose its opposite.
    // If we add backface culling, this will be backfacing
    ecNormal *= -one_float;
  }

  // Light calculations
  vec3 totalAmbient = vec3(0, 0, 0);
  vec3 totalDiffuse = vec3(0, 0, 0);
  vec3 totalSpecular = vec3(0, 0, 0);
  for (int i = 0; i < 8; i++) {
    if (lightCount == i) break;
    
    vec3 lightPos = lightPosition[i].xyz;
    bool isDir = zero_float < lightPosition[i].w;
    float spotCos = lightSpot[i].x;
    float spotExp = lightSpot[i].y;
    
    vec3 lightDir;
    float falloff;
    float spotf;
      
    if (isDir) {
      falloff = one_float;
      lightDir = -one_float * lightNormal[i];
    } else {
      falloff = falloffFactor(lightPos, ecVertex, lightFalloff[i]);
      lightDir = normalize(lightPos - ecVertex);
    }
  
    spotf = spotExp > zero_float ? spotFactor(lightPos, ecVertex, lightNormal[i],
                                              spotCos, spotExp)
                                 : one_float;
    
    if (any(greaterThan(lightAmbient[i], zero_vec3))) {
      totalAmbient += lightAmbient[i] * falloff;
    }
    
    if (any(greaterThan(lightDiffuse[i], zero_vec3))) {
      totalDiffuse += lightDiffuse[i] * falloff * spotf *
                       lambertFactor(lightDir, ecNormal);
    }
    
    if (any(greaterThan(lightSpecular[i], zero_vec3))) {
      totalSpecular += lightSpecular[i] * falloff * spotf *
                       blinnPhongFactor(lightDir, ecVertex, ecNormal, shininess);
    }
  }
  
  // Calculating final color as result of all lights (plus emissive term).
  // Transparency is determined exclusively by the diffuse component.
  vertColor = vec4(totalAmbient, 0) * ambient +
              vec4(totalDiffuse, 1) * color +
              vec4(totalSpecular, 0) * specular +
              vec4(emissive.rgb, 0);
              
}

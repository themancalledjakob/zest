/**
 * ArduinoManager
 * Why the hell should you have this not one single class, but an extended Manager class, does this make sense?
 * Yep, because maybe maybe there will be two computers for the two screens. one that receives arduino data & sends osc
 * and one that receives osc.
 * by having it split into two Managers, we can easily have the same code running on both machines without effort.
 * so it's potentially superhandy. though potentially redundant. who cares.
 */

class ArduinoManager extends Manager {

  Arduino arduino;

  ArduinoManager(TRIPLE_BIKE mother){
    // Prints out the available serial ports.
    println(Arduino.list());
    println("choosing this one: " + findArduino());
    arduino = new Arduino(mother, findArduino(), 57600);
  }

  @Override
  public void update(){
    update(true);
  }

  @Override
  public void update(boolean real){
    if(real)
    for(int i = 0; i < conf.values; i++){
      val[i] = arduino.analogRead(i);
      val[i] = i == 2 ? val[i]*2 : val[i];
      total += val[i];
      totalIndy[i] += val[i];
    }
    if(!real){
      val[0] = (int)map(mouseX, 0, width, 0, 60);
      val[1] = (int)map(mouseY, 0, height, 0, 60);
      val[2] = (int)map(mouseY, 0, height, 0, 60);

      for(int i = 0; i < conf.values; i++){
        total += val[i];
        totalIndy[i] += val[i];
      }
    }
    super.update();
  }

  // tools
  private String findArduino() {
    for( int i = 0; i < Arduino.list().length; ++i){
      if(Arduino.list()[i].contains("/dev/tty.usbmodem"))
        return Arduino.list()[i];
    }
    return "no arduino found. do it by hand.";
  }
}
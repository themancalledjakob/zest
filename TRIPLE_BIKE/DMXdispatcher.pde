/**
 * DMXDispatcher
 * dispatches dmx
 */

class DMXdispatcher {

  DmxP512 dmxOutput;
  int universeSize=128;

  boolean DMXPRO=true;
  String DMXPRO_PORT="/dev/tty.usbserial-EN159832";//case matters ! on windows port must be upper cased.
  int DMXPRO_BAUDRATE=115000;

  DMXdispatcher(TRIPLE_BIKE mother){
    dmxOutput=new DmxP512(mother, universeSize, false);  

    if(DMXPRO){
      dmxOutput.setupDmxPro(DMXPRO_PORT,DMXPRO_BAUDRATE);
    }
  }

  void update(int[] mapData){

    int r[] = {
      int(min(255.0,0.7*mapData[0]+0.0*mapData[1])),
      0,
      int(min(255.0,0.0*mapData[0]+0.3*mapData[1]))
    };

    int b[] = {
      int(min(255.0,0.0*mapData[2]+0.1*mapData[1])),
      int(min(255.0,0.07*mapData[2]+0.0*mapData[1])),
      int(min(255.0,0.7*mapData[2]+0.0*mapData[1]))
    };

    dmxOutput.set(1, 255);//(mapData[0]));
    dmxOutput.set(2, r[0]);
    dmxOutput.set(3, r[1]);
    dmxOutput.set(4, r[2]);

    dmxOutput.set(21, 255);//(mapData[0]));
    dmxOutput.set(22, b[0]);
    dmxOutput.set(23, b[1]);
    dmxOutput.set(24, b[2]);

    // dmxOutput.set(1, 255);//(mapData[0]));
    // dmxOutput.set(2, int(1.0*mapData[0]));
    // dmxOutput.set(3, int(0.0*mapData[0]));
    // dmxOutput.set(4, int(0.0*mapData[0]));

    // dmxOutput.set(11, 255);//(mapData[1]));
    // dmxOutput.set(12, int(0.5*mapData[1]));
    // dmxOutput.set(13, int(0.1*mapData[1]));
    // dmxOutput.set(14, int(0.5*mapData[1]));

    // dmxOutput.set(21, 255);//(mapData[2]));
    // dmxOutput.set(22, int(0.0*mapData[2]));
    // dmxOutput.set(23, int(0.1*mapData[2]));
    // dmxOutput.set(24, int(1.0*mapData[2]));
  }
}
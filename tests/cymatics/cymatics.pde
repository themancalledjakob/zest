PShader deform;

void setup() {
  size(900, 1400, P2D);
  deform = loadShader("cymatics.glsl");
  deform.set("resolution",float(width), float(height));
  shader(deform);
}

void draw() {
  deform.set("time", millis() / 1000.0);
  deform.set("mouse", float(mouseX), float(mouseY));
  
  rect(0, 0, width, height);
}
PImage tex;
PShader deform;

void setup() {
  size(900, 1400, P2D);
  
  ((PGraphics2D)g).textureWrap(Texture.REPEAT);   
  tex = loadImage("tex1.jpg");
 
  deform = loadShader("deform.glsl");
  deform.set("resolution",float(width), float(height));
  shader(deform);
}

void draw() {
	deform.set("textureSampler",tex);
  deform.set("time", millis() / 1000.0);
  deform.set("mouse", float(mouseX), float(mouseY));
  
  image(tex, 0, 0, width, height);
}
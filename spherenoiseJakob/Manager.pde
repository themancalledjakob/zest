/**
 * Manager
 * all managers do this
 */

class Manager {

  int val[];      // Data received
  float smoothVal[]; 

  Conf conf = new Conf();

  class Conf {
    float smoothing = 0.95;
    int min = 0;
    int max = 360;
    int values = 2;
  }

  public void setup(){
    val = new int[conf.values];
    smoothVal = new float[conf.values]; 
  }
  
  public void update(){
  }

  int[] getRawValues()
  {
    return val;
  }

  float[] getSmoothValues(){
    return getSmoothValues(false);
  }
  float[] getSmoothValues(boolean mapped)
  {
    float out[] = new float[conf.values];

    for(int i = 0; i < conf.values; ++i){
      out [i] = smoothVal[i] * conf.smoothing + (1.0-conf.smoothing) * val[i];

      if(mapped) {
        out[i] = map(out[i], conf.min, conf.max, 0.0, 1.0);
      }
    }

    return out;
  }
}



/*

// Wiring / Arduino Code
// Code for sensing a switch status and writing the value to the serial port.

int switchPin = 4;                       // Switch connected to pin 4

void setup() {
  pinMode(switchPin, INPUT);             // Set pin 0 as an input
  Serial.begin(9600);                    // Start serial communication at 9600 bps
}

void loop() {
  if (digitalRead(switchPin) == HIGH) {  // If switch is ON,
    Serial.write(1);               // send 1 to Processing
  } else {                               // If the switch is not ON,
    Serial.write(0);               // send 0 to Processing
  }
  delay(100);                            // Wait 100 milliseconds
}

*/
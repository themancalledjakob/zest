import cc.arduino.*;
import processing.serial.*;

int subdivisionLevel = 8; // number of times the icosahedron will be subdivided
int dim = 300; // the grid dimensions of the heightmap
int blurFactor = 3; // the blur for the displacement map (to make it smoother)
float resizeFactor = 0.2; // the resize factor for the displacement map (to make it smoother)
float displaceStrength = 0.75; // the displace strength of the GLSL shader displacement effect

PShape sphere; // PShape to hold the geometry, textures, texture coordinates etc.
PShader displace; // GLSL shader

PImage[] images = new PImage[13]; // array to hold 2 input images
int txt1 = 0;
int txt2 = 1;
int currentColorMap = 1; // variable to keep track of the current colorMap

ArduinoManager arduino = new ArduinoManager(this);

boolean pleaseSetWindowPosition;

// boolean sketchFullScreen() {
//   return true;
// }

void setup() {
  // if ( !isGL() )   removeFrameBorder();
  size(1024, 1280, P3D); // use the P3D OpenGL renderer
  pleaseSetWindowPosition = true;

  // load the images from the _Images folder (relative path from this sketch's folder)
  for(int i = 0; i < images.length; i++){
    String nummer = i < 9 ? "0" + str(i+1) : str(i+1);
    nummer += i == 11 ? ".png" : ".jpg";
    images[i] = loadImage("txture/" + nummer);
  }
  displace = loadShader("displaceFrag.glsl", "displaceVert.glsl"); // load the PShader with a fragment and a vertex shader
  displace.set("displaceStrength", displaceStrength); // set the displaceStrength
  displace.set("colorMap", images[currentColorMap]); // set the initial colorMap
  arduino.setup();
  sphere = createIcosahedron(subdivisionLevel); // create the subdivided icosahedron PShape (see custom creation method) and put it in the global sphere reference
}

void draw() {
  background(244);
  if (pleaseSetWindowPosition) {
    frame.setLocation(1280, 0);
    pleaseSetWindowPosition = false;
  }

  arduino.update();
  // image(images[txt2],0,0,width,height);
  pointLight(255, 0, 255, 2*(mouseX-width/2), 2*(mouseY-height/2), 500); // required for texLight shader
  pushMatrix();
  translate(width/2, height/2); // translate to center of the screen
//  rotateX(radians(60)); // fixed rotation of 60 degrees over the X axis
  rotateZ(arduino.getSmoothValues()[1]*0.01f); // dynamic frameCount-based rotation over the Z axis
  // rotateY(frameCount*0.05); // dynamic frameCount-based rotation over the Z axis

//  background(0); // black background
  perspective(PI/3.0, (float) width/height, 0.1, 1000000); // perspective for close shapes
  scale(200); // scale by 200
  displace.set("time", arduino.getSmoothValues()[1] * 0.01f); // feed time to the GLSL shader
  // shader(displace); // use shader
  shape(sphere); // display the PShape

  // write the fps and the current colorMap in the top-left of the window
  frame.setTitle("super amazing many frames, like " + int(frameRate));
  popMatrix();
  fill(255,0,0);
  rect(0,0,500,500);

  fill(255,0,0);
  rect(0,800,1024,40);

  fill(255,0,0);
  rect(0,1200,1024,20);
}

void keyPressed() {
  if(key == ' ') { println("swiitch");txt1++; txt1%=images.length; txt2++; txt2%=images.length; };
  
  if (key == 'c') { currentColorMap = ++currentColorMap%images.length; displace.set("colorMap", images[currentColorMap]); } // cycle through colorMaps (set variable and set colorMap in PShader)
}

// void removeFrameBorder() {
//   frame.removeNotify();
//   frame.setUndecorated(true);
//   frame.addNotify();
// }

import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import cc.arduino.*; 
import processing.serial.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class SpherenoiseJakob extends PApplet {






int subdivisionLevel = 8; // number of times the icosahedron will be subdivided
int dim = 300; // the grid dimensions of the heightmap
int blurFactor = 3; // the blur for the displacement map (to make it smoother)
float resizeFactor = 0.2f; // the resize factor for the displacement map (to make it smoother)
float displaceStrength = 0.75f; // the displace strength of the GLSL shader displacement effect

PShape sphere; // PShape to hold the geometry, textures, texture coordinates etc.
PShader displace; // GLSL shader

PImage[] images = new PImage[13]; // array to hold 2 input images
int txt1 = 0;
int txt2 = 1;
int currentColorMap = 1; // variable to keep track of the current colorMap

ArduinoManager arduino = new ArduinoManager(this);

boolean windowPositionSet;

// boolean sketchFullScreen() {
//   return true;
// }

public void setup() {
  // if ( !isGL() )   removeFrameBorder();
  size(1024, 1280, P3D); // use the P3D OpenGL renderer
  // windowPositionSet = false;

  // load the images from the _Images folder (relative path from this sketch's folder)
  for(int i = 0; i < images.length; i++){
    String nummer = i < 9 ? "0" + str(i+1) : str(i+1);
    nummer += i == 11 ? ".png" : ".jpg";
    images[i] = loadImage("txture/" + nummer);
  }
  displace = loadShader("displaceFrag.glsl", "displaceVert.glsl"); // load the PShader with a fragment and a vertex shader
  displace.set("displaceStrength", displaceStrength); // set the displaceStrength
  displace.set("colorMap", images[currentColorMap]); // set the initial colorMap
  arduino.setup();
  sphere = createIcosahedron(subdivisionLevel); // create the subdivided icosahedron PShape (see custom creation method) and put it in the global sphere reference
}

public void draw() {
  background(244);
  if (!windowPositionSet) {
    frame.setLocation(1280, 0);
    windowPositionSet = true;
  }

  arduino.update();
  // image(images[txt2],0,0,width,height);
  pointLight(255, 0, 255, 2*(mouseX-width/2), 2*(mouseY-height/2), 500); // required for texLight shader
  pushMatrix();
  translate(width/2, height/2); // translate to center of the screen
//  rotateX(radians(60)); // fixed rotation of 60 degrees over the X axis
  rotateZ(arduino.getSmoothValues()[1]*0.01f); // dynamic frameCount-based rotation over the Z axis
  // rotateY(frameCount*0.05); // dynamic frameCount-based rotation over the Z axis

//  background(0); // black background
  perspective(PI/3.0f, (float) width/height, 0.1f, 1000000); // perspective for close shapes
  scale(200); // scale by 200
  displace.set("time", arduino.getSmoothValues()[1] * 0.01f); // feed time to the GLSL shader
  // shader(displace); // use shader
  shape(sphere); // display the PShape

  // write the fps and the current colorMap in the top-left of the window
  frame.setTitle("super amazing many frames, like " + PApplet.parseInt(frameRate));
  popMatrix();
  fill(255,0,0);
  rect(0,0,500,500);

  fill(255,0,0);
  rect(0,800,1024,40);

  fill(255,0,0);
  rect(0,1200,1024,20);
}

public void keyPressed() {
  if(key == ' ') { println("swiitch");txt1++; txt1%=images.length; txt2++; txt2%=images.length; };
  
  if (key == 'c') { currentColorMap = ++currentColorMap%images.length; displace.set("colorMap", images[currentColorMap]); } // cycle through colorMaps (set variable and set colorMap in PShader)
}

// void removeFrameBorder() {
//   frame.removeNotify();
//   frame.setUndecorated(true);
//   frame.addNotify();
// }
/**
 * ArduinoManager
 * Why the hell should you have this not one single class, but an extended Manager class, does this make sense?
 * Yep, because maybe maybe there will be two computers for the two screens. one that receives arduino data & sends osc
 * and one that receives osc.
 * by having it split into two Managers, we can easily have the same code running on both machines without effort.
 * so it's potentially superhandy. though potentially redundant. who cares.
 */

class ArduinoManager extends Manager {

  Arduino arduino;

  ArduinoManager(SpherenoiseJakob mother){
    // Prints out the available serial ports.
    println(Arduino.list());
    println("choosing this one: " + findArduino());
    arduino = new Arduino(mother, findArduino(), 57600);
  }

  @Override
  public void update(){
    for(int i = 0; i < conf.values; i++){
      val[i] = arduino.analogRead(i);
    }
  }

  // tools
  private String findArduino() {
    for( int i = 0; i < Arduino.list().length; ++i){
      if(Arduino.list()[i].contains("/dev/tty.usbmodem"))
        return Arduino.list()[i];
    }
    return "no arduino found. do it by hand.";
  }
}



/*

// Wiring / Arduino Code
// Code for sensing a switch status and writing the value to the serial port.

int switchPin = 4;                       // Switch connected to pin 4

void setup() {
  pinMode(switchPin, INPUT);             // Set pin 0 as an input
  Serial.begin(9600);                    // Start serial communication at 9600 bps
}

void loop() {
  if (digitalRead(switchPin) == HIGH) {  // If switch is ON,
    Serial.write(1);               // send 1 to Processing
  } else {                               // If the switch is not ON,
    Serial.write(0);               // send 0 to Processing
  }
  delay(100);                            // Wait 100 milliseconds
}

*/

// ported to Processing 2.0b8 by Amnon Owed (10/05/2013)
// from code by Gabor Papp (13/03/2010): http://git.savannah.gnu.org/cgit/fluxus.git/tree/libfluxus/src/GraphicsUtils.cpp
// based on explanation by Paul Bourke (01/12/1993): http://paulbourke.net/geometry/platonic
// using vertex/face list by Craig Reynolds: http://paulbourke.net/geometry/platonic/icosahedron.vf

class Icosahedron {
  ArrayList <PVector> positions = new ArrayList <PVector> ();
  ArrayList <PVector> normals = new ArrayList <PVector> ();
  ArrayList <PVector> texCoords = new ArrayList <PVector> ();

  Icosahedron(int level) {
    float sqrt5 = sqrt(5);
    float phi = (1 + sqrt5) * 0.5f;
    float ratio = sqrt(10 + (2 * sqrt5)) / (4 * phi);
    float a = (1 / ratio) * 0.5f;
    float b = (1 / ratio) / (2 * phi);

    PVector[] vertices = {
      new PVector( 0,  b, -a), 
      new PVector( b,  a,  0), 
      new PVector(-b,  a,  0), 
      new PVector( 0,  b,  a), 
      new PVector( 0, -b,  a), 
      new PVector(-a,  0,  b), 
      new PVector( 0, -b, -a), 
      new PVector( a,  0, -b), 
      new PVector( a,  0,  b), 
      new PVector(-a,  0, -b), 
      new PVector( b, -a,  0), 
      new PVector(-b, -a,  0)
    };

    int[] indices = { 
      0,1,2,    3,2,1,
      3,4,5,    3,8,4,
      0,6,7,    0,9,6,
      4,10,11,  6,11,10,
      2,5,9,    11,9,5,
      1,7,8,    10,8,7,
      3,5,2,    3,1,8,
      0,2,9,    0,7,1,
      6,9,11,   6,10,7,
      4,11,5,   4,8,10
    };

    for (int i=0; i<indices.length; i += 3) {
      makeIcosphereFace(vertices[indices[i]],  vertices[indices[i+1]],  vertices[indices[i+2]],  level);
    }
  }

  public void makeIcosphereFace(PVector a, PVector b, PVector c, int level) {

    if (level <= 1) {
      
      // cartesian to spherical coordinates
      PVector ta = new PVector(atan2(a.z, a.x) / TWO_PI + 0.5f, acos(a.y) / PI);
      PVector tb = new PVector(atan2(b.z, b.x) / TWO_PI + 0.5f, acos(b.y) / PI);
      PVector tc = new PVector(atan2(c.z, c.x) / TWO_PI + 0.5f, acos(c.y) / PI);

      // texture wrapping coordinate limits
      float mint = 0.25f;
      float maxt = 1 - mint;

      // fix north and south pole textures
      if ((a.x == 0) && ((a.y == 1) || (a.y == -1))) {
        ta.x = (tb.x + tc.x) / 2;
        if (((tc.x < mint) && (tb.x > maxt)) || ((tb.x < mint) && (tc.x > maxt))) { ta.x += 0.5f; }
      } else if ((b.x == 0) && ((b.y == 1) || (b.y == -1))) {
        tb.x = (ta.x + tc.x) / 2;
        if (((tc.x < mint) && (ta.x > maxt)) || ((ta.x < mint) && (tc.x > maxt))) { tb.x += 0.5f; }
      } else if ((c.x == 0) && ((c.y == 1) || (c.y == -1))) {
        tc.x = (ta.x + tb.x) / 2;
        if (((ta.x < mint) && (tb.x > maxt)) || ((tb.x < mint) && (ta.x > maxt))) { tc.x += 0.5f; }
      }

      // fix texture wrapping
      if ((ta.x < mint) && (tc.x > maxt)) {
        if (tb.x < mint) { tc.x -= 1; } else { ta.x += 1; }
      } else if ((ta.x < mint) && (tb.x > maxt)) {
        if (tc.x < mint) { tb.x -= 1; } else { ta.x += 1; }
      } else if ((tc.x < mint) && (tb.x > maxt)) {
        if (ta.x < mint) { tb.x -= 1; } else { tc.x += 1; }
      } else if ((ta.x > maxt) && (tc.x < mint)) {
        if (tb.x < mint) { ta.x -= 1; } else { tc.x += 1; }
      } else if ((ta.x > maxt) && (tb.x < mint)) {
        if (tc.x < mint) { ta.x -= 1; } else { tb.x += 1; }
      } else if ((tc.x > maxt) && (tb.x < mint)) {
        if (ta.x < mint) { tc.x -= 1; } else { tb.x += 1; }
      }

      addVertex(a, a, ta);
      addVertex(c, c, tc);
      addVertex(b, b, tb);

    } else { // level > 1

      PVector ab = midpointOnSphere(a, b);
      PVector bc = midpointOnSphere(b, c);
      PVector ca = midpointOnSphere(c, a);

      level--;
      makeIcosphereFace(a, ab, ca, level);
      makeIcosphereFace(ab, b, bc, level);
      makeIcosphereFace(ca, bc, c, level);
      makeIcosphereFace(ab, bc, ca, level);
    }
  }

  public void addVertex(PVector p, PVector n, PVector t) {
    positions.add(p);
    normals.add(n);
    t.set(1.0f-t.x, 1.0f-t.y, t.z);
    texCoords.add(t);
  }

  public PVector midpointOnSphere(PVector a, PVector b) {
    PVector midpoint = PVector.add(a, b);
    midpoint.mult(0.5f);
    midpoint.normalize();
    return midpoint;
  }
}

public PShape createIcosahedron(int level) {
  // the icosahedron is created with positions, normals and texture coordinates in the above class
  Icosahedron ico = new Icosahedron(level);

  textureMode(NORMAL); // set textureMode to normalized (range 0 to 1);
  
  PShape mesh = createShape(); // create the initial PShape
  mesh.beginShape(TRIANGLES); // define the PShape type: TRIANGLES
  mesh.noStroke();
  mesh.texture(images[0]); // set the texture
  // put all the vertices, uv texture coordinates and normals into the PShape
  for (int i=0; i<ico.positions.size(); i++) {
    PVector p = ico.positions.get(i);
    PVector t = ico.texCoords.get(i);
    PVector n = ico.normals.get(i);
    mesh.normal(n.x, n.y, n.z);
    mesh.vertex(p.x, p.y, p.z, t.x, t.y);
  }
  mesh.endShape();

  return mesh; // our work is done here, return DA MESH! ;-)
}

/**
 * Manager
 * all managers do this
 */

class Manager {

  int val[];      // Data received
  float smoothVal[]; 

  Conf conf = new Conf();

  class Conf {
    float smoothing = 0.95f;
    int min = 0;
    int max = 360;
    int values = 2;
  }

  public void setup(){
    val = new int[conf.values];
    smoothVal = new float[conf.values]; 
  }
  
  public void update(){
  }

  public int[] getRawValues()
  {
    return val;
  }

  public float[] getSmoothValues(){
    return getSmoothValues(false);
  }
  public float[] getSmoothValues(boolean mapped)
  {
    float out[] = new float[conf.values];

    for(int i = 0; i < conf.values; ++i){
      out [i] = smoothVal[i] * conf.smoothing + (1.0f-conf.smoothing) * val[i];

      if(mapped) {
        out[i] = map(out[i], conf.min, conf.max, 0.0f, 1.0f);
      }
    }

    return out;
  }
}



/*

// Wiring / Arduino Code
// Code for sensing a switch status and writing the value to the serial port.

int switchPin = 4;                       // Switch connected to pin 4

void setup() {
  pinMode(switchPin, INPUT);             // Set pin 0 as an input
  Serial.begin(9600);                    // Start serial communication at 9600 bps
}

void loop() {
  if (digitalRead(switchPin) == HIGH) {  // If switch is ON,
    Serial.write(1);               // send 1 to Processing
  } else {                               // If the switch is not ON,
    Serial.write(0);               // send 0 to Processing
  }
  delay(100);                            // Wait 100 milliseconds
}

*/
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "SpherenoiseJakob" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}


uniform sampler2D colorMap;

varying vec4 vertTexCoord;

void main() {
  vec4 ratat = texture2D(colorMap, vertTexCoord.st);
  gl_FragColor = vec4(ratat.r, ratat.b, ratat.g, 1.0);
}


import dmxP512.*;
import processing.serial.*;
import cc.arduino.*;

/*

 GLSL Heightmap by Amnon Owed (May 2013)
 https://github.com/AmnonOwed
 http://vimeo.com/amnon
 
 Creating a heightmap with a separate color and displacement map.
 The color and displacement are handled in GLSL (fragment and vertex) shaders.
 The input textures for both the color and the displacement can be changed in realtime.
 
 c = cycle through the color maps
 d = cycle through the displacement maps
 
 Built with Processing 2.0b8 / 2.0b9 / 2.0 Final
 
 Photographs by Folkert Gorter (@folkertgorter / http://superfamous.com/) made available under a CC Attribution 3.0 license.
 
 */

int dim = 300; // the grid dimensions of the heightmap
int blurFactor = 3; // the blur for the displacement map (to make it smoother)
float resizeFactor = 1.0; // the resize factor for the displacement map (to make it smoother)
float displaceStrength = 0.35; // the displace strength of the GLSL shader displacement effect

float mouserX = 0;
float mouserY = 0;

PShape heightMap; // PShape to hold the geometry, textures, texture coordinates etc.
PShader displace; // GLSL shader

PImage[] colorMaps = new PImage[19]; // array to hold 3 colorMaps
PImage[] displacementMaps = new PImage[19]; // array to hold 3 displacementMap 
int currentColorMap, currentDisplacementMap = 2; // variables to keep track of the current maps (also used for setting them)
Manager bikes = new ArduinoManager(this);
DMXdispatcher dmx  = new DMXdispatcher(this);

int totalBikes = 0;

void setup() {
  bikes.setup();
  // bikes = new ArduinoManager(this);
  // dmx
  size(1280, 720, P3D); // use the P3D OpenGL renderer

  // load the images from the _Images folder (relative path from this sketch's folder)
  colorMaps[0] = loadImage("txture/01.jpg");
  colorMaps[1] = loadImage("txture/02.jpg");
  colorMaps[2] = loadImage("txture/03.jpg");
  colorMaps[3] = loadImage("txture/04.jpg");
  colorMaps[4] = loadImage("txture/05.jpg");
  colorMaps[5] = loadImage("txture/06.jpg");
  // colorMaps[6] = loadImage("txture/07.jpg");
  // colorMaps[7] = loadImage("txture/08.jpg");
  // colorMaps[8] = loadImage("9.jpg");
  // colorMaps[9] = loadImage("10.jpg");
  // colorMaps[10] = loadImage("11.jpg");
  // colorMaps[11] = loadImage("12.jpg");
  // colorMaps[12] = loadImage("13.jpg");
  // colorMaps[13] = loadImage("14.jpg");
  // colorMaps[14] = loadImage("15.jpg");
  // colorMaps[15] = loadImage("16.jpg");
  // colorMaps[16] = loadImage("17.jpg");
  // colorMaps[17] = loadImage("18.jpg");
  // colorMaps[18] = loadImage("3.jpg");

  image(colorMaps[currentColorMap], 0, 0, displayWidth, displayHeight);
  // create the displacement maps from the images
  displacementMaps[0] = imageToDisplacementMap(colorMaps[0]);
  displacementMaps[1] = imageToDisplacementMap(colorMaps[1]);
  displacementMaps[2] = imageToDisplacementMap(colorMaps[2]);
  displacementMaps[3] = imageToDisplacementMap(colorMaps[3]);
  displacementMaps[4] = imageToDisplacementMap(colorMaps[4]);
  displacementMaps[5] = imageToDisplacementMap(colorMaps[5]);
  // displacementMaps[6] = imageToDisplacementMap(colorMaps[6]);
  // displacementMaps[7] = imageToDisplacementMap(colorMaps[7]);
  // displacementMaps[8] = imageToDisplacementMap(colorMaps[8]);
  // displacementMaps[9] = imageToDisplacementMap(colorMaps[9]);
  // displacementMaps[10] = imageToDisplacementMap(colorMaps[10]);
  // displacementMaps[11] = imageToDisplacementMap(colorMaps[11]);
  // displacementMaps[12] = imageToDisplacementMap(colorMaps[12]);
  // displacementMaps[13] = imageToDisplacementMap(colorMaps[13]);
  // displacementMaps[14] = imageToDisplacementMap(colorMaps[14]);
  // displacementMaps[15] = imageToDisplacementMap(colorMaps[15]);
  // displacementMaps[16] = imageToDisplacementMap(colorMaps[16]);
  // displacementMaps[17] = imageToDisplacementMap(colorMaps[17]);
  // displacementMaps[18] = imageToDisplacementMap(colorMaps[18]);



  displace = loadShader("displaceFrag.glsl", "displaceVert.glsl"); // load the PShader with a fragment and a vertex shader
  resetMaps(); // set the color and displacement maps
  heightMap = createPlane(dim, dim); // create the heightmap PShape (see custom creation method) and put it in the global heightMap reference
}
void draw() {

  frustum(0, width, height, 0, -1000, 1000);
  bikes.update();
  // println("bikes 0:" + bikes.getDmxValues()[0] + " 1:" + bikes.getDmxValues()[1] + " 2:" + bikes.getDmxValues()[2]);
  // println("smooth 0:" + bikes.getSmoothValues(true)[0] + " 1:" + bikes.getSmoothValues(true)[1] + " 2:" + bikes.getSmoothValues(true)[2]);
  // println("total " + bikes.getTotal());
  float inputtest = map(mouseX,0,width,1,120);

  float bikeres1 = max(0.75,map(bikes.getSmoothValues(true)[2],0,0.4,10.0,0.75));
  float bikeres2 = max(0.75,map(bikes.getSmoothValues(true)[2],0,0.4,10.0,0.75));
  dmx.update(bikes.getDmxValues());
  displace.set("resolution",bikeres1, bikeres2);
  // displace.set("resolution",map(mouseX,0,width,0.75,10.0), map(mouseY,0,height,0.75,10.0));

  displace.set("time", (bikes.getTotal() / 1000.0));
  displace.set("mouse", 1.0,1.0); //map(mouseX,0,width,0.0,1.0), map(mouseY,0,height,0.0,1.0));
  
  float displaceStrength2 = max(0.0,map(bikes.getSmoothValues(true)[1],0,0.4,0.0,0.2));
  displace.set("displaceStrength", displaceStrength2); // set the displaceStrength
  // translate(0, -200+mouseY, -mouseX);

  pointLight(255, 255, 255, 2*(-width/2), 2*(-height/2), mouseX); // required for texLight shader

  translate(width/2, height/2); // translate to center of the screen
  pointLight(255, 255, 255, 0, 0, 500); // required for texLight shader
  // rotateX(radians(60)); // fixed rotation of 60 degrees over the X axis
  // rotateZ(float(int(bikes.getTotal()))*0.001); // dynamic frameCount-based rotation over the Z axis
  rotateX(
     radians(60.0 + cos(bikes.getTotalIndy()[1] * 0.01 * map(bikes.getSmoothValues(true)[1],0,0.4,1,1.5) ) * 10.0)
    );
  rotateY(
     radians(cos(bikes.getTotalIndy()[0] * 0.01 * map(bikes.getSmoothValues(true)[0],0,0.4,1,1.5) ) * 10.0)
  );
  rotateZ(
   float(int(bikes.getTotalIndy()[2]))*0.003
  );

  //background(0); // black background
  perspective(PI/3.0, (float) width/height, 0.1, 1000000); // perspective for close shapes

  int totalBikes_tmp = int(bikes.getTotal())%50000;
  if(totalBikes_tmp < totalBikes){
    currentColorMap += 2;
    currentColorMap %= colorMaps.length;
  }
  int currentColorMap2  = currentColorMap+1;
  currentColorMap2 %= colorMaps.length;

  totalBikes = totalBikes_tmp;
  float scaleMe = float(totalBikes)/50000;
  scaleMe = pow(scaleMe,2); // power the value to get it hanging around in the low values for longer
  scaleMe = map(scaleMe,0,1,10,5000);
  scaleMe = 750; // WATCH OUT< THIS IS JUST FOR CHECKING

  scale(scaleMe); // scale by 750 (the model itself is unit length

  shader(displace); // use shader
  displace.set("colorMap", colorMaps[currentColorMap]);
  displace.set("colorMap2", colorMaps[currentColorMap2]);
  float interpol = min(1.0,bikes.getSmoothValueSum()*6.0); // last number is telling you how hard you have to paddle to get the other texture
  // interpol = sin(millis()*0.01)*0.5+0.5;

  float fade = 1.0; // fade the landscape out at the end
  if(scaleMe > 4500){
    fade = map(scaleMe,4500,5000,1.0,0.0);
  }

  displace.set("interpol", interpol);
  displace.set("fade", fade); 
  // displace.set("translateMe", float(int(bikes.getTotal()))*0.1);
  shape(heightMap); // display the PShape
  // displace.set("colorMap", colorMaps[(currentColorMap+1)%5]);

  // spp.setFill(color(0, 0, 255));
  // spp.setStroke(false);

  // sphere(1.0); // display the PShape


  // write the fps, the current colorMap and the current displacementMap in the top-left of the window
  // text("input is  " + inputtest, 180, 280);
  // text("used like this is  " + usedlike, 180, 380);
  frame.setTitle(" " + int(frameRate) + " | colorMap: " + currentColorMap + " / " + currentColorMap2 + " | total: " + totalBikes + " interpol " + interpol );
}

// custom method to create a PShape plane with certain xy dimensions
PShape createPlane(int xsegs, int ysegs) {

  // STEP 1: create all the relevant data

  ArrayList <PVector> positions = new ArrayList <PVector> (); // arrayList to hold positions
  ArrayList <PVector> texCoords = new ArrayList <PVector> (); // arrayList to hold texture coordinates

  float usegsize = 1 / (float) xsegs; // horizontal stepsize
  float vsegsize = 1 / (float) ysegs; // vertical stepsize

  for (int x=0; x<xsegs; x++) {
    for (int y=0; y<ysegs; y++) {
      float u = x / (float) xsegs;
      float v = y / (float) ysegs;

      // generate positions for the vertices of each cell (-0.5 to center the shape around the origin)
      positions.add( new PVector(u-0.5, v-0.5, 0) );
      positions.add( new PVector(u+usegsize-0.5, v-0.5, 0) );
      positions.add( new PVector(u+usegsize-0.5, v+vsegsize-0.5, 0) );
      positions.add( new PVector(u-0.5, v+vsegsize-0.5, 0) );

      // generate texture coordinates for the vertices of each cell
      texCoords.add( new PVector(u, v) );
      texCoords.add( new PVector(u+usegsize, v) );
      texCoords.add( new PVector(u+usegsize, v+vsegsize) );
      texCoords.add( new PVector(u, v+vsegsize) );
    }
  }

  // STEP 2: put all the relevant data into the PShape

    textureMode(NORMAL); // set textureMode to normalized (range 0 to 1);
  PImage tex = loadImage("14.jpg");

  PShape mesh = createShape(); // create the initial PShape
  mesh.beginShape(QUADS); // define the PShape type: QUADS
  mesh.noStroke();
  mesh.texture(tex); // set a texture to make a textured PShape
  // put all the vertices, uv texture coordinates and normals into the PShape
  for (int i=0; i<positions.size(); i++) {
    PVector p = positions.get(i);
    PVector t = texCoords.get(i);
    mesh.vertex(p.x, p.y, p.z, t.x, t.y);
  }
  mesh.endShape();

  return mesh; // our work is done here, return DA MESH! ;-)
}

// a separate resetMaps() method, so the images can be change dynamically
void resetMaps() {
  displace.set("colorMap", colorMaps[currentColorMap]);
  displace.set("displacementMap", displacementMaps[currentDisplacementMap]);
}

// convenience method to create a smooth displacementMap
PImage imageToDisplacementMap(PImage img) {
  PImage imgCopy = img.get(); // get a copy so the original remains intact
  imgCopy.resize(int(imgCopy.width*resizeFactor), int(imgCopy.height*resizeFactor)); // resize
  if (blurFactor >= 1) { 
    imgCopy.filter(BLUR, blurFactor);
  } // apply blur
  return imgCopy;
}

void keyPressed() {
  if (key == 'c') { 
    currentColorMap = ++currentColorMap%colorMaps.length; 
    resetMaps();
  } // cycle through colorMaps (set variable and call resetMaps() method)
  if (key == 'd') { 
    currentDisplacementMap = ++currentDisplacementMap%displacementMaps.length; 
    resetMaps();
  } // cycle through displacementMaps (set variable and call resetMaps() method)
}

